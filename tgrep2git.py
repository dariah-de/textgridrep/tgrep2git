import os
import zipfile
from pathlib import Path

import click
import git
import gitlab
import requests
from gitlab.exceptions import GitlabGetError, GitlabHttpError, GitlabAuthenticationError
from rdflib import Graph
from tgclients.aggregator import Aggregator
from tgclients.config import TextgridConfig
from tgclients.crud import TextgridCRUD
from tgclients.search import TextgridSearch
from xsdata.formats.dataclass.serializers import XmlSerializer
from xsdata.formats.dataclass.serializers.config import SerializerConfig

WORKDIR = './work'
GITLAB_DEFAULT_HOST = 'https://gitlab.gwdg.de'
GITLAB_DEFAULT_NAMESPACE = 'dariah-de/textgridrep/digitale-bibliothek'
TEXTGRID_DEFAULT_HOST = 'https://textgridlab.org'

@click.command()
@click.argument('textgrid_uri')
@click.option('--token', default=lambda: os.environ.get('GITLAB_TOKEN', ''), 
    help='A gitlab auth token. Defaults to environment variable GITLAB_TOKEN')
@click.option('--host', default=lambda: os.environ.get('GITLAB_HOST', GITLAB_DEFAULT_HOST), 
    help='URL to gitlab instance. Also as environment variable GITLAB_HOST', show_default=GITLAB_DEFAULT_HOST)  
@click.option('--namespace', default=lambda: os.environ.get('GITLAB_NAMESPACE', GITLAB_DEFAULT_NAMESPACE), 
    help='Path to parent gitlab project, where the new project should be created. Also as environment variable "GITLAB_NAMESPACE"', 
    show_default=GITLAB_DEFAULT_NAMESPACE) 
@click.option('--tghost', default=lambda: os.environ.get('TEXTGRID_HOST', TEXTGRID_DEFAULT_HOST),
    help='URL to textgrid instance. Also as environment variable TEXTGRID_HOST', show_default=TEXTGRID_DEFAULT_HOST)
def cli(textgrid_uri, token, host, namespace, tghost):
    """Download an aggregation via its textgridURI from aggregator and upload to gitlab project"""

    if(token==''):
        click.secho('Please provide an gitlab auth token', fg='red')
        exit(0)

    ###
    # setup
    ###
    config = TextgridConfig(tghost)
    agg = Aggregator()
    search = TextgridSearch()
    crud = TextgridCRUD()
    # create workdir
    Path(WORKDIR).mkdir(parents=True, exist_ok=True)

    ###
    # get metadata and find title and depiction
    ###
    metadata = search.info(textgrid_uri)
    # gitlab does not allow colon in project name -> remove
    title = metadata.result[0].object_value.generic.provided.title[0]
    click.echo(f'title is: {title}')
    project_name = title.replace(',', '')
    click.echo(f'gitlab project name is: {project_name}')
    slug = project_name.lower().replace(' ', '-')
    click.echo(f'slug is: {slug}')

    if(os.path.exists(f'{WORKDIR}/{slug}')):
        click.secho(f'Found local directory "{WORKDIR}/{slug}". Please remove', fg='red')
        exit(1)

    ###
    # download zipped contents from aggregator
    ###
    click.echo(f"Downloading zip file for {textgrid_uri} from aggregator!")
    response = agg.zip(textgrid_uri)
    zipfilename = f'{WORKDIR}/{textgrid_uri[9:]}.zip'
    with open(zipfilename, 'wb') as f:
      f.write(response.content)

    # get and download depiction
    img_path = get_avatar_uri_from_tgmeta(metadata)

    ###
    # create project on gitlab
    ###
    description = f'Imported from https://textgridrep.org/{textgrid_uri} with [tgrep2git](https://gitlab.gwdg.de/dariah-de/textgridrep/tgrep2git)'
    gitlab_repo_url = create_gitlab_repo(token=token, host=host, namespace=namespace, 
                        slug=slug, img_path=img_path, title=project_name, description=description)

    ###
    # create local git dir, unzip file from aggregator there und push to gitlab
    ###
    gitdir = f'{WORKDIR}/{slug}'
    repo = git.Repo.init(gitdir)

    with zipfile.ZipFile(zipfilename, 'r') as zip_ref:
        zip_ref.extractall(gitdir)

    repo.git.add(all=True)
    origin = repo.create_remote('origin', gitlab_repo_url)

    repo.index.commit(f'imported {textgrid_uri} from tgrep via aggregator')
    origin.push(refspec='main:main')

    click.secho(f'DONE. Check the import at: {host}/{namespace}/{slug}', fg='cyan' )


def create_gitlab_repo(token: str, host: str, namespace: str, slug: str, img_path: str, title: str, description: str) -> str:
    """Creates project and repo for slug on gitlab

    Args:
        token(str): a gitlab auth token
        host(str): the gitlab host
        namespace(str): path to parent project on gitlab
        slug (str): the slug (from project title)
        img_path (str): path to avatar image
        title(str): title
        description (str): project description

    Returns:
        str the ssh+git url to the repo
    """
    gl = gitlab.Gitlab(url=host, private_token=token)
    group_id = gl.groups.list(search=namespace)[0].id
    project_name_with_namespace = f'{namespace}/{slug}'

    try:
        project = gl.projects.get(project_name_with_namespace)
        click.secho(f'Project "{slug}" already exists on {host}/{namespace}', fg='red')
        exit(1)
    except GitlabGetError:
        click.echo(f'Project "{slug}" not on gitlab. Creating now.')
        project = gl.projects.create({'name': title, 'path': slug, 'namespace_id': group_id, 'visibility': 'public'})

    project.description = description

    if(img_path != ''):
        project.avatar = open(img_path, 'rb')

    try:
        project.save()
    except GitlabAuthenticationError:
        click.secho(f'Authentication error - is your token valid for project "{namespace}"?', fg='red')
        exit(1)

    return project.ssh_url_to_repo


def get_avatar_uri_from_tgmeta(tgmeta):
    """get the tg:depiction relation from tgmeta rdf and download thumb from digilib"""

    if(not tgmeta.result[0].object_value.relations.rdf):
        click.secho("No RDF found in metadata, skipping avatar image download")
        return ''

    # serialize rdf-xml to string
    config = SerializerConfig(pretty_print=True)
    serializer = XmlSerializer(config=config)
    rdf = serializer.render(tgmeta.result[0].object_value.relations.rdf)
    # parse as rdf with rdflib
    g = Graph().parse(data=rdf, format='xml')
    q = """
        PREFIX tg: <http://textgrid.info/relation-ns#>
        SELECT ?img
        WHERE { ?s tg:depiction ?img. }
    """
    # sparql
    img_uri = ""
    for r in g.query(q):
        img_uri = r['img']

    click.echo(f'depiction uri: {img_uri}')

    ###
    # download avatar img as thumb from digilib
    ###
    thumbUrl = f'https://textgridlab.org/1.0/digilib/rest/IIIF/{img_uri}/full/,250/0/native.jpg'
    res = requests.get(thumbUrl)
    img_path = f'{WORKDIR}/{img_uri[9:]}.jpeg'
    with open(img_path, "wb") as f:
      f.write(res.content)

    return img_path