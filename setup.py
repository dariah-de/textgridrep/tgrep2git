from setuptools import setup

setup(
    name='tgrep2git',
    version='0.1.0',
    py_modules=['tgrep2git'],
    install_requires=[
        'click',
        'rdflib',
        'python-gitlab',
        'GitPython',
        'tgclients @ https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-python-clients/-/archive/4dbbeac3b78e452fa9c44785d35a423aef9f6d60/textgrid-python-clients-4dbbeac3b78e452fa9c44785d35a423aef9f6d60.tar.gz'
    ],
    entry_points={
        'console_scripts': [
            'tgrep2git = tgrep2git:cli',
        ],
    },
)