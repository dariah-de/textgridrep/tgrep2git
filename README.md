# tgrep2git

Import collections from the [TextGrid-Repository](https://textgridrep.org) into
[own gitlab projects](https://gitlab.gwdg.de/dariah-de/textgridrep/digitale-bibliothek) using the 
[aggregator export](https://textgridlab.org/doc/services/submodules/aggregator/docs/zip.html).

## setup

    python3 -m venv venv
    . venv/bin/activate
    pip install --editable .


## run

help:

    tgrep2git --help

Import ["Alice im Wunderland"](https://textgridrep.org/browse/kv2p.0) into gitlab. You need a gitlab token with write access for the 
parent project (defaults to https://gitlab.gwdg.de/dariah-de/textgridrep/digitale-bibliothek):

    tgrep2git --token=CHANGEME textgrid:kv2p.0

or set token with environment variable:

    GITLAB_TOKEN=CHANGEME tgrep2git textgrid:kv2p.0

for an overview of all options and environment variables consult the help.

You can also overwrite gitlab-host, gitlab-namespace and
textgrid-host with environment variables and options.

## config file

I suggest creating a file `.env` and setting all parameters there, 
exporting this to environment before running the script. You can copy and
use [env.tmpl](./env.tmpl) for this.

### complete example with environment config

copy the .env file

    cp env.tmpl .env

edit `.env` and provide the `GITLAB_TOKEN`, then:

    . venv/bin/activate
    set -o allexport; source .env; set +o allexport
    tgrep2git textgrid:vqmw.0
